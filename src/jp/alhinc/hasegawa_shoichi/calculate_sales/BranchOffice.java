package jp.alhinc.hasegawa_shoichi.calculate_sales;

public class BranchOffice {
	private String officeCode;
	private String officeName;
	private long totalSales = 0;

	BranchOffice(String officeCode, String officeName){
		this.officeCode = officeCode;
		this.officeName = officeName;
	}

	public String getOfficeCode(){
		return this.officeCode;
	}
	public long getTotalSales(){
		return this.totalSales;
	}
	public String getOfficeName(){
		return this.officeName;
	}

	public void addTotalSalses(long sales){
		totalSales += sales;
	}
}
