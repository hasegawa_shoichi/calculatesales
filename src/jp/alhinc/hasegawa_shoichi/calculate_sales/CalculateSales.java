package jp.alhinc.hasegawa_shoichi.calculate_sales;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CalculateSales {

	public static void main(String[] args) {
		String branchFileName = null;
		try{
			branchFileName = args[0];
			if(args.length > 1){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		branchFileName +="//branch.lst";
		File branchFile = new File(branchFileName);

		if(!(branchFile.exists())){
			System.out.println("支店定義ファイルが存在しません");
			return;
		}

		if(!(checkBranchFormat(branchFile))){
			System.out.println("支店定義ファイルのフォーマットが不正です");
			return;
		}

		ArrayList<BranchOffice> branchOffices = new ArrayList<BranchOffice>();

		addBranch(branchOffices,branchFile);

		ArrayList<File> earningsFiles = getEarningsFiles(args[0]);

		if(!(checkSerialNumber(earningsFiles))){
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}

		for(int i = 0; i < earningsFiles.size(); i++){
			if(!(checkEarningsFormat(earningsFiles.get(i)))){
				return;
			}
		}

		for(int j = 0; j < earningsFiles.size(); j++){
			if(!(addSalse(branchOffices, earningsFiles.get(j)))){
				return;
			}
		}

		/*ファイル出力*/
		writeFile(branchOffices, args[0]);
	}

	static void addBranch(ArrayList<BranchOffice> branchOffices, File branchFile) {
		ArrayList<String> branchCodes = new ArrayList<String>();
		Scanner brScanner;
		try {
			brScanner = new Scanner(branchFile);
			try{
				while(brScanner.hasNextLine()){
					branchCodes.add(brScanner.nextLine());
				}
			}finally{
				if(brScanner != null){
					brScanner.close();
				}
			}
		}catch (FileNotFoundException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
		for(int i = 0; i < branchCodes.size(); i ++){
			String[] branch = branchCodes.get(i).split(",");
			branchOffices.add(new BranchOffice(branch[0], branch[1]));
		}
	}

	static ArrayList<File> getEarningsFiles(String path){
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String name) {
				if(name.matches("[0-9]{8}\\.rcd")){
					return true;
				}
				return false;
			}
		};
		File[] f = new File(path).listFiles(filter);
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(f));
		return files;
	}

	static boolean addSalse(ArrayList<BranchOffice> branchOffices, File earningsFile) {
		boolean canAddSalse = false;
		Scanner efScanner;
		try{
			efScanner = new Scanner(earningsFile);
			try {
				String branchCode = efScanner.nextLine();
				long earnings = Long.parseLong(efScanner.nextLine());
				for(int j = 0; j < branchOffices.size(); j++){
					String s = branchOffices.get(j).getOfficeCode();
					if(s.equals(branchCode)){
						if(!(checkEarningsOverflow(earnings,branchOffices.get(j)))){
							canAddSalse = false;
							System.out.println("合計金額が10桁を超えました");
							break;
					}
					branchOffices.get(j).addTotalSalses(earnings);
					canAddSalse = true;
					break;
					}else if(j == branchOffices.size() -1 && !(s.equals(branchCode))){
						System.out.println(earningsFile.getName() +  "の支店コードが不正です");
					}
				}
			}finally{
				if(efScanner != null){
					efScanner.close();
				}
			}
		}catch(FileNotFoundException e){
			System.out.println("予期せぬエラーが発生しました");
		}catch(NumberFormatException e){
			System.out.println("予期せぬエラーが発生しました");
			canAddSalse = false;
			return canAddSalse;
		}
		return canAddSalse;
	}

	static void writeFile(ArrayList<BranchOffice> branchOffices, String args){
		String writeFilePath = args + "//branch.out";
		File file = new File(writeFilePath);
		PrintWriter pw = null;
		try{
			pw = new PrintWriter(new BufferedWriter(new FileWriter(file,false)));
			for(int i = 0; i < branchOffices.size(); i++){
				String oc = branchOffices.get(i).getOfficeCode();
				String on = branchOffices.get(i).getOfficeName();
				long es = branchOffices.get(i).getTotalSales();
				pw.printf("%s,%s,%d%n",oc,on,es);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}finally{
			if(pw != null){
				pw.close();
			}
		}
	}

	static boolean checkBranchFormat(File branchFile){
		boolean permitFormat = true;
		ArrayList<String> branchCode = new ArrayList<String>();
		Scanner brScanner;
		try {
			brScanner = new Scanner(branchFile);
			try{
				while(brScanner.hasNextLine()){
					String bc = brScanner.nextLine();
					if(!(bc.matches("^[0-9]{3},.*"))){
						permitFormat = false;
						return permitFormat;
					}else if(bc.split(",").length > 2){
						permitFormat = false;
						return permitFormat;
					}
					branchCode.add(bc);
				}
			}finally{
				if(brScanner != null){
					brScanner.close();
				}
			}
		}catch (FileNotFoundException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
		return permitFormat;
	}

	static boolean checkSerialNumber(ArrayList<File> files){
		boolean permitSerial = true;
		ArrayList<Integer> serialNumber = new ArrayList<Integer>();
		for(int i = 0; i < files.size(); i++){
			String s =files.get(i).getName().replaceAll("^[0]*","");
			s = s.replaceAll("\\.rcd$", "");
			serialNumber.add(Integer.parseInt(s));
			if(!(files.get(i).isFile())){
				if(i == files.size() - 1){
				permitSerial = true;
				files.remove(i);
				break;
				}
				permitSerial = false;
			}
		}
		for(int j = 1; j < serialNumber.size() + 1; j++){
			if(!(j == serialNumber.get(j-1))){
				permitSerial = false;
				break;
			}
		}
		return permitSerial;
	}

	static boolean checkEarningsFormat(File earningsFile) {
		boolean permitFormat = true;
		ArrayList<String> earningsData = new ArrayList<String>();
		Scanner edScanner;
		try{
			edScanner = new Scanner(earningsFile);
			try{
				while(edScanner.hasNextLine()){
					earningsData.add(edScanner.nextLine());
					}
				if(!(earningsData.size() == 2)){
					System.out.println(earningsFile.getName() + "のフォーマットが不正です");
					permitFormat = false;
				}else if(!(earningsData.get(1).matches("^\\d{1,10}$"))){
					System.out.println("予期せぬエラーが発生しました");
					permitFormat = false;
				}
			}finally{
				if(edScanner != null){
					edScanner.close();
				}
			}
		}catch(FileNotFoundException e){
			System.out.println("予期せぬエラーが発生しました");
		}catch(NumberFormatException e){
			System.out.println("予期せぬエラーが発生しました");
			permitFormat = false;
			return permitFormat;
		}
		return permitFormat;
	}
	static boolean checkEarningsOverflow(long earnings,BranchOffice branchOffice){
		long brSalse = branchOffice.getTotalSales();
		long todayEarnings = earnings;
		if((brSalse + todayEarnings) >= 10000000000L){
			return false;
		}
		return true;
	}
}